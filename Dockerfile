FROM ubuntu:14.04
MAINTAINER CorGinia keisuke satomi <mlh38048@yahoo.co.jp>

RUN apt-get update && apt-get install -y python curl
WORKDIR /root
RUN curl -O https://bootstrap.pypa.io/get-pip.py
RUN python get-pip.py
RUN pip install awscli
